/*
 * World.h
 *
 *  Created on: Oct 20, 2014
 *      Author: alexander
 */

#ifndef WORLD_H_
#define WORLD_H_

#include <Box2D/Box2D.h>
#include <map>
#include <vector>
#include <string>
#include "Level.h"
#include "GameObjectDef.h"
#include "Model.h"
#include "ModelDef.h"
#include "Player.h"
#include "Car.h"
#include "Wheel.h"

using std::vector;
using std::map;
using std::pair;
using std::string;

namespace gem {
class GameObject;
class Car;
class Player;
class ResourceManager;
class World {
public:
	World();
	virtual ~World();

	GameObject*  createGameObject( GameObjectDef* );
	Model* createModel( ModelDef* );
	Model* getModelById( const string family, size_t id );
	GameObject* getPlayer();
	void setPlayer( GameObject* newPlayer );
	void step();
	ResourceManager* rm;
	friend class Game;

private:
	b2World _physicsWorld;
	vector<GameObject*> _gameObjects;
	map<const string, map<const string, Model*>> _models;
	GameObject* _player;
	map<const string, Soul*> _souls;
};

} /* namespace gem */
#endif /* WORLD_H_ */
