/*
 * JointAnchors.h
 *
 *  Created on: Nov 5, 2014
 *      Author: alexander
 */

#ifndef MJOINTDEF_H_
#define MJOINTDEF_H_

#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>
#include <string>

using std::string;

namespace gem {

struct MJointDef {
    string name;
    b2JointType type;
    b2JointDef* jointDef;
    string bodyAid;
    string bodyBid;
};

}

#endif /* MJOINTDEF_H_ */
