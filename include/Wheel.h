/*
 * Wheel.h
 *
 *  Created on: Dec 7, 2014
 *      Author: alexander
 */

#ifndef WHEEL_H_
#define WHEEL_H_

#include <map>
#include <string>
#include "Soul.h"
#include "GameObject.h"
#include "Car.h"

namespace gem {

class Wheel : public Soul {
public:
    Wheel();
    virtual ~Wheel();

    void act( GameObject* carrier );

private:
    map<string, sf::Keyboard::Key> Key = { {"up", sf::Keyboard::W},
                                               {"down", sf::Keyboard::S},
                                               {"left", sf::Keyboard::A},
                                               {"right", sf::Keyboard::D},
                                               {"up2", sf::Keyboard::Up},
                                               {"down2", sf::Keyboard::Down},
                                               {"left2", sf::Keyboard::Left},
                                               {"right2", sf::Keyboard::Right}};
};

} /* namespace gem */

#endif /* WHEEL_H_ */
