/*
 * Camera.h
 *
 *  Created on: Oct 8, 2014
 *      Author: alexander
 */

#ifndef CAMERA_H_
#define CAMERA_H_

namespace gem {

class Camera {
public:
	Camera();
	virtual ~Camera();

private:

};

} /* namespace gem */

#endif /* CAMERA_H_ */
