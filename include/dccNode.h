namespace gem {
/// DCCList node
template<typename T>
struct dccNode {
	T* prev;
	T* next;
	T value;
};

}
