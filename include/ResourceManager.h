/*
 * ResourceManager.h
 *
 *  Created on: Oct 31, 2014
 *      Author: alexander
 */

#ifndef RESOURCEMANAGER_H_
#define RESOURCEMANAGER_H_

#include <rapidxml-1.13/rapidxml.hpp>
#include <rapidxml-1.13/rapidxml_utils.hpp>
#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>
#include <sstream>
#include <map>
#include <string>
#include "Utils.h"
#include "Level.h"
#include "GameObjectDef.h"
#include "PhysicMaterial.h"
#include "ModelDef.h"
#include "MJointDef.h"

using std::map;
using std::pair;
using std::string;

namespace gem {

class ResourceManager {
public:
	ResourceManager();
	virtual ~ResourceManager();

	void loadPhysicMaterials( const string file );
	void loadModelDef( const string xmlFile, const string svgFile );
	void loadLevel( const string file );

	ModelDef* getModelDef( const string name );
	PhysicMaterial* getPhysicMaterial( const string name );
	Level* getLevel( const string name );
	float getC();

private:
	float _c;
	map<const string, Level*> _levels;
	map<const string, PhysicMaterial*> _physicMaterials;
	map<const string, ModelDef*> _modelDefs;

	rapidxml::xml_node<>* getSiblingWithId( rapidxml::xml_node<>* node, const char* id );
	rapidxml::xml_node<>* getChildWithId( rapidxml::xml_node<>* node, const char* id );
	sf::Vector2f getSVGSize( rapidxml::xml_node<>* firstNode );
	sf::Vector2f getSVGPos( rapidxml::xml_node<>* firstNode );
	void loadPath( rapidxml::xml_node<>* pathNode, sf::Vector2f borderSize, GameObjectDef* goDef );
	void loadRect( rapidxml::xml_node<>* rectNode, sf::Vector2f borderSize, GameObjectDef* goDef );
};

}

#endif /* RESOURCEMANAGER_H_ */
