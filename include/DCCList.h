/*
 * DCCList.h
 *
 *  Created on: Oct 24, 2014
 *      Author: alexander
 */

#ifndef DCCLIST_H_
#define DCCLIST_H_

#include <stdlib.h>
#include "dccNode.h"

namespace gem {



/// Doubly connected cyclic list
template<typename T>
class DCCList {
public:

	DCCList():
		_size( 0 ),
		_head( nullptr )
	{}

	void add( T element );
	void remove( dccNode<T>* element );
	dccNode<T>* head();
	size_t size();

private:
	size_t _size;
	dccNode<T>* _head;
};

} /* namespace gem */

#endif /* DCCLIST_H_ */
