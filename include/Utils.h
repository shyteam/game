/*
 * LevelLoader.h
 *
 *  Created on: Oct 20, 2014
 *      Author: alexander
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <SFML/Graphics.hpp>
#include <vector>
#include <string>
#include "rapidxml-1.13/rapidxml.hpp"
#include "rapidxml-1.13/rapidxml_utils.hpp"
#include "Level.h"

using rapidxml::xml_document;
using rapidxml::xml_node;
using rapidxml::xml_attribute;
using std::vector;
using std::string;

namespace gem {

enum POINT_POSITION { UNDER, ON, ABOVE };
enum GENERAL_POINTS { NO, ONE, INF };

class Utils {
public:
	/// Simple line y = k*x + b
	class Line {
	public:
		Line();
		/// Line through two points
		Line( sf::Vector2f A, sf::Vector2f B );
		/// Retunrns point position according to the line:
		/// UNDER, ON or ABOVE
		POINT_POSITION pointIs( sf::Vector2f D );
		/// Returns general point of the lines
		sf::Vector2f intersects( Line l );
		/// Returns true if point in section of the line
		bool isPointInSection( sf::Vector2f X, sf::Vector2f A, sf::Vector2f B );
		float k;
		float b;
	};

	/// Returns z proection of a vector product of the vectors
	static float vectorProduct( sf::Vector2f A, sf::Vector2f B  );
	/// Is the point V in the triangle ABC
	static bool inTriangle( sf::Vector2f V, sf::Vector2f A, sf::Vector2f B, sf::Vector2f C );
	static string getFileName( const string filePath );
};

} /* namespace gem */

#endif /* LEVELLOADER_H_ */
