#ifndef LEVEL_H_
#define LEVEL_H_

#include <SFML/Graphics.hpp>
#include <vector>
#include "GameObjectDef.h"
#include "ModelDef.h"

using std::vector;

namespace gem {

/// Contains all information about Level
struct Level {
	vector<GameObjectDef*> groundDefs;
	vector<ModelDef*> modelDefs;
	char* name;
	sf::Vector2f size;
};

}

#endif
