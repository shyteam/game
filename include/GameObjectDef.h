#ifndef GAMEOBJECTDEF_H_
#define GAMEOBJECTDEF_H_

#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>
#include <vector>
#include <string>
#include "PhysicMaterial.h"
#include "Model.h"

using std::string;
using std::vector;

namespace gem {

/// Contains all information about GameObject
struct GameObjectDef {
    string id;
	vector<string> soulNames;
	vector<sf::Drawable*> drawables;
	b2FixtureDef fixture;
	b2BodyDef bodyDef;
    map<const string, float> params;

	void loadPhysMaterial( const PhysicMaterial* physMaterial ) {
	    fixture.density = physMaterial->density;
	    fixture.friction = physMaterial->friction;
	    fixture.restitution = physMaterial->restitution;
	}
};

}

#endif
