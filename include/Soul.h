/*
 * Soul.h
 *
 *  Created on: Oct 29, 2014
 *      Author: alexander
 */

#ifndef SOUL_H_
#define SOUL_H_

namespace gem {
class GameObject;
class Soul {
public:
	virtual ~Soul(){};
	virtual void act( GameObject* go ) = 0;
};

} /* namespace gem */

#endif /* SOUL_H_ */
