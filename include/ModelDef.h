/*
 * ModelDef.h
 *
 *  Created on: Nov 14, 2014
 *      Author: alexander
 */

#ifndef MODELDEF_H_
#define MODELDEF_H_

#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>
#include <map>
#include <string>
#include "GameObjectDef.h"
#include "MJointDef.h"

using std::map;
using std::string;

namespace gem {

struct ModelDef {
    string family;
    sf::Vector2f sfPos;
    sf::Vector2f sfSize;
    b2Vec2 b2Pos;
    vector<GameObjectDef*> gameObjectDefs;
    vector<MJointDef*> jointDefs;
};

} /* namespace gem */

#endif /* MODELDEF_H_ */
