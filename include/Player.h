/*
 * Player.h
 *
 *  Created on: Oct 29, 2014
 *      Author: alexander
 */

#ifndef PLAYER_H_
#define PLAYER_H_

#include <map>
#include <string>
#include "Soul.h"
#include "GameObject.h"

namespace gem {

class Player: public Soul {
public:
	Player();
	~Player();
	void act( GameObject* carrier );
	void jump( GameObject* carrier );
	void moveLeft( GameObject* carrier );
	void moveRight( GameObject* carrier );
	void bend( GameObject* carrier );

private:
	map<string, sf::Keyboard::Key> Key = { {"up", sf::Keyboard::W},
	                                       {"down", sf::Keyboard::S},
	                                       {"left", sf::Keyboard::A},
	                                       {"right", sf::Keyboard::D},
	                                       {"up2", sf::Keyboard::Up},
	                                       {"down2", sf::Keyboard::Down},
	                                       {"left2", sf::Keyboard::Left},
	                                       {"right2", sf::Keyboard::Right}};

};

} /* namespace gem */

#endif /* PLAYER_H_ */
