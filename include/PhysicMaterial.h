#ifndef PHYSICMATERIAL_H_
#define PHYSICMATERIAL_H_

namespace gem {

struct PhysicMaterial {
    float density;
    float friction;
    float restitution;
};

}

#endif
