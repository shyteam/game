/*
 * GameObject.h
 *
 *  Created on: Oct 20, 2014
 *      Author: alexander
 */

#ifndef GAMEOBJECT_H_
#define GAMEOBJECT_H_

#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>
#include <string>
#include "World.h"
#include "Soul.h"
#include "Model.h"

using std::string;

namespace gem {
class World;
class GameObject : public sf::Drawable {
public:
    GameObject();
    virtual ~GameObject();
    void update( float c, sf::Vector2f worldSize );
    string local_id;
    Model* model;
    Soul* soul;
    b2Body* body;
    sf::Drawable* drawable;
    World* world;
    map<const string, float> params;

private:
    void draw( sf::RenderTarget& target, sf::RenderStates states ) const;
};

} /* namespace gem */
#endif /* GAMEOBJECT_H_ */
