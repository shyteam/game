/*
 * Car.h
 *
 *  Created on: Nov 28, 2014
 *      Author: alexander
 */

#ifndef CAR_H_
#define CAR_H_

#include <string>
#include "ResourceManager.h"
#include "Soul.h"
#include "GameObject.h"
#include "ModelDef.h"

namespace gem {

class Car : public Soul {
public:
    Car();
    virtual ~Car();
    void act( GameObject* carrier );
    bool isPlayerInCar;

private:
    map<string, sf::Keyboard::Key> Key = { {"up", sf::Keyboard::W},
                                           {"down", sf::Keyboard::S},
                                           {"left", sf::Keyboard::A},
                                           {"right", sf::Keyboard::D},
                                           {"up2", sf::Keyboard::Up},
                                           {"down2", sf::Keyboard::Down},
                                           {"left2", sf::Keyboard::Left},
                                           {"right2", sf::Keyboard::Right}};

};

} /* namespace gem */

#endif /* CAR_H_ */
