/*
 * Game.h
 *
 *  Created on: Oct 20, 2014
 *      Author: alexander
 */

#ifndef GAME_H_
#define GAME_H_

#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include "ResourceManager.h"
#include "World.h"
#include "Level.h"
#include "GameObject.h"
#include "Soul.h"
#include "Player.h"

using std::vector;
using std::size_t;

namespace gem {

class Game {
public:
	Game();
	virtual ~Game();

	void giveWindow( sf::RenderWindow* window );
	void giveResourceManager( ResourceManager* rm );

	/// Loads all specified levels to heap
	void makeLevel( Level* level );


	/// Creates World
	/// Loads levels to World in specified order
	/// Calls go() function which contains main game loop and returns at
	/// the end of the level only (in normal mode) so start() will be able
	/// to load next level to World and to call go() again
	void start();

	/// Contains main game loop
	/// Performs drawing calls and physics calculations
	/// Have an opportunity to pause the game and call pause()
	void go();

private:

	/// Provides pause menu
	/// Have an opportunity to stop the game and call pause()
	void pause();

	/// Cleans all resources
	/// If game stops control returns to main() in main.cpp
	void stop();

	World* _world;
	sf::RenderWindow* _window;
	ResourceManager* _rm;
	Level* _level;
};

} /* namespace gem */
#endif /* GAME_H_ */
