/*
 * DCCList.cpp
 *
 *  Created on: Oct 24, 2014
 *      Author: alexander
 */

#include "DCCList.h"

namespace gem {

template<typename T>
void gem::DCCList<T>::add( T element ) {
	dccNode<T>* newNode = new dccNode<T>();
	if ( _size == 0 ) {
		newNode->prev = newNode;
		newNode->next = newNode;
		newNode->value = element;
		_head = newNode;
	}
	else {
		newNode->prev = _head->prev;
		newNode->next = _head;
		_head->prev = newNode;
	}
	_size++;
}
/*
template<typename T>
void gem::DCCList<T>::remove( dccNode<T>* element ) {
	if ( _size > 1 ) {
		if ( element == _head ) {
			_head = _head->next;
		}
		element->prev->next = element->next;
		element->next->prev = element->prev;
		_size--;
		delete element;
	}
	else if ( _size == 1 ) {
		_size--;
		delete element;
	}

}

template<typename T>
gem::dccNode<T>* gem::DCCList<T>::head() {
	return _head;
}

template<typename T>
size_t gem::DCCList<T>::size() {
	return _size;
}*/

} /* namespace gem */
