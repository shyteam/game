/*
 * Model.h
 *
 *  Created on: Nov 14, 2014
 *      Author: alexander
 */

#ifndef MODEL_H_
#define MODEL_H_

#include <string>
#include <map>

using std::map;
using std::string;

namespace gem {

class GameObject;

struct Model {
    string id;
    string family;
    map<const string, GameObject*> gameObjects;
};

}

#endif /* MODEL_H_ */


