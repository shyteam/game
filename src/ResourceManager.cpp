/*
 * ResourceManager.cpp
 *
 *  Created on: Oct 31, 2014
 *      Author: alexander
 */

#include <ResourceManager.h>

namespace gem {

ResourceManager::ResourceManager(): _c( 100.0f ), _levels(), _physicMaterials(), _modelDefs() {
}

ResourceManager::~ResourceManager() {
    // TODO Auto-generated destructor stub
}

void ResourceManager::loadPhysicMaterials( const string file ) {
    rapidxml::file<> xmlFile( file.data() );
    rapidxml::xml_document<> xmlDoc;
    xmlDoc.parse<0>( xmlFile.data() );
    for ( auto pNode = xmlDoc.first_node(); pNode != 0; pNode = pNode->next_sibling() ) {
        PhysicMaterial* pMat = new PhysicMaterial();
        pMat->density = atof( pNode->first_attribute( "density" )->value() );
        pMat->friction = atof( pNode->first_attribute( "friction" )->value() );
        pMat->restitution = atof( pNode->first_attribute( "restitution" )->value() );
        char* name = pNode->name();
        _physicMaterials.insert( std::pair<const string, PhysicMaterial*>( name, pMat ) );
    }
}

void ResourceManager::loadModelDef( const string xmlFileName, const string svgFileName ) {
    rapidxml::file<> xmlFile( xmlFileName.data() );
    rapidxml::xml_document<> xmlDoc;
    xmlDoc.parse<0>( xmlFile.data() );
    rapidxml::file<> svgFile( svgFileName.data() );
    rapidxml::xml_document<> svgDoc;
    svgDoc.parse<0>( svgFile.data() );
    auto svgN = svgDoc.first_node();
    sf::Vector2f size = getSVGSize( svgN );
    ModelDef* mDef = new ModelDef();
    mDef->sfSize = size;
    mDef->family = Utils::getFileName( xmlFileName );
    // Load shapes
    auto gN = svgN->first_node( "g" );
    for ( auto xmlN = xmlDoc.first_node( "shape" ); xmlN != 0; xmlN = xmlN->next_sibling( "shape" ) ) {
        GameObjectDef* goDef = new GameObjectDef();
        goDef->id = xmlN->first_attribute( "id" )->value();
        auto shapeN = getChildWithId( gN, goDef->id.data() );
        goDef->loadPhysMaterial( _physicMaterials[xmlN->first_attribute( "phys_material" )->value()] );
        if ( strcmp( shapeN->name(), "path" ) == 0 )
            loadPath( shapeN, size, goDef );
        else
            loadRect( shapeN, size, goDef );

        auto soulsAttr = xmlN->first_attribute( "souls" );
        if ( soulsAttr != 0 ) {
            string soulsStr = soulsAttr->value();
            std::stringstream ss(soulsStr);
            std::string soul;
            while (std::getline(ss, soul, ',')) {
                goDef->soulNames.push_back( soul );
            }
            for ( auto i = soulsAttr->next_attribute(); i != nullptr; i = i->next_attribute() ) {
                goDef->params[i->name()] = atof( i->value() );
            }
        }
        mDef->gameObjectDefs.push_back( goDef );
    }
    // Load joints
    for ( auto xmlN = xmlDoc.first_node( "joint" ); xmlN != 0; xmlN = xmlN->next_sibling( "joint" ) ) {
        MJointDef* mjDef = new MJointDef();
        mjDef->name = xmlN->first_attribute( "id" )->value();
        string strType = xmlN->first_attribute( "type" )->value();
        mjDef->bodyAid = xmlN->first_attribute( "bodyA" )->value();
        mjDef->bodyBid = xmlN->first_attribute( "bodyB" )->value();
        if ( strType == "revolute" ) {
            mjDef->type = e_revoluteJoint;
            auto jointN = getChildWithId( gN, mjDef->name.data() );
            b2RevoluteJointDef* b2jDef = new b2RevoluteJointDef();
            sf::Vector2f sfPos = getSVGPos( jointN );
            b2jDef->localAnchorA = b2Vec2( sfPos.x / _c, ( size.y - sfPos.y ) / _c );
            mjDef->jointDef = b2jDef;
        }
        mDef->jointDefs.push_back( mjDef );
    }
    _modelDefs.insert( pair<const string, ModelDef*>( mDef->family, mDef ) );
}

PhysicMaterial* ResourceManager::getPhysicMaterial( const string name ) {
    return _physicMaterials[name];
}

void ResourceManager::loadLevel( const string file ) {
    rapidxml::file<> svgFile( file.data() );
    rapidxml::xml_document<> svgDoc;
    svgDoc.parse<0>( svgFile.data() );
    auto svgN = svgDoc.first_node();
    Level* level = new Level();
    level->size = getSVGSize( svgN );
    auto gN = svgN->first_node( "g" );
    // Load grounds
    for ( auto shapeN = gN->first_node( "path" ); shapeN != 0; shapeN = shapeN->next_sibling( "path" ) ) {
        GameObjectDef* goDef = new GameObjectDef();
        goDef->id = "ground";
        goDef->loadPhysMaterial( _physicMaterials["ground"] );
        loadPath( shapeN, level->size, goDef );
        level->groundDefs.push_back( goDef );
    }

    for ( auto shapeN = gN->first_node( "rect" ); shapeN != 0; shapeN = shapeN->next_sibling( "rect" ) ) {
        string mName = shapeN->first_attribute( "inkscape:label" )->value();
        ModelDef* mDef = new ModelDef( *_modelDefs[mName] );
        mDef->sfPos = getSVGPos( shapeN );
        mDef->b2Pos = b2Vec2( mDef->sfPos.x / _c, ( level->size.y - mDef->sfPos.y - mDef->sfSize.y ) / _c );
        level->modelDefs.push_back( mDef );
    }

    _levels.insert( pair<const string, Level*>( Utils::getFileName( file ), level ) );
}

Level* ResourceManager::getLevel( const string name ) {
    return _levels[name];
}

rapidxml::xml_node<>* ResourceManager::getChildWithId( rapidxml::xml_node<>* node, const char* id ) {
    return getSiblingWithId( node->first_node(), id );
}


rapidxml::xml_node<>* ResourceManager::getSiblingWithId( rapidxml::xml_node<>* node, const char* id ) {
    for ( auto pNode = node; pNode != 0; pNode = pNode->next_sibling() ) {
        if ( strcmp( pNode->first_attribute( "id" )->value(), id ) == 0 ) {
            return pNode;
        }
    }
    return 0;
}

void ResourceManager::loadPath( rapidxml::xml_node<>* pathNode, sf::Vector2f borderSize, GameObjectDef* goDef ) {    // Read vertices of the path
    vector<sf::Vertex> vArray;
    sf::Vertex vertex;
    char* dStr = pathNode->first_attribute( "d" )->value() + 2;
    int n = 0;
    size_t minVertexI = 0;
    while ( sscanf( dStr, "%f,%f%n", &vertex.position.x, &vertex.position.y, &n ) != 0 ) {
        dStr += n;
        vertex.color = sf::Color( rand() % 256, rand() % 256, rand() % 256 );
        vertex.texCoords.x = ( int ) vertex.position.x % 64;
        vertex.texCoords.y = ( int ) vertex.position.y % 64;
        vArray.push_back( vertex );
        if ( vArray.back().position.x < vArray[minVertexI].position.x )
            minVertexI = vArray.size() - 1;
    }
    size_t vCount = vArray.size();

    // b2Shape
    b2Vec2* vertices = new b2Vec2[vCount];
    for ( size_t i = 0; i < vCount; i++ )
        vertices[i].Set( ( vArray[i].position.x ) / _c, ( borderSize.y - vArray[i].position.y ) / _c );
    if ( strcmp( pathNode->first_attribute( "inkscape:label" )->value(), "ground" ) == 0 ) {
        b2ChainShape* chainShape = new b2ChainShape;
        chainShape->CreateLoop( vertices, vCount );
        goDef->fixture.shape = chainShape;
    } else {
        b2PolygonShape* polygonShape = new b2PolygonShape;
        polygonShape->Set( vertices, vCount );
        goDef->fixture.shape = polygonShape;
    }

    // Compute traversal direction of the path
    size_t k2 = ( minVertexI + 1 ) % vArray.size();
    size_t k0 = ( vArray.size() + minVertexI - 1 ) % vArray.size();
    float traversalDirection = (
            Utils::vectorProduct(
                    vArray[k0].position - vArray[minVertexI].position,
                    vArray[k2].position - vArray[minVertexI].position ) > 0.0f ? 1.0f : -1.0f );

    // Triangulation
    vector<sf::Vertex> tempArray( vArray );
    sf::VertexArray* trianglesArray = new sf::VertexArray( sf::Triangles );
    size_t realVCount = vCount;
    size_t v1I = 0;
    while ( realVCount > 3 ) {
        if ( tempArray[v1I].position == sf::Vector2f( 0.0f, 0.0f ) )
            continue;
        size_t v2I = 0;
        size_t uhoVI = 0;
        for ( size_t j = 1; j < tempArray.size() - 2; j++ ) {
            uhoVI = ( v1I + j ) % vCount;
            bool f = false;
            if ( tempArray[uhoVI].position == sf::Vector2f( 0.0f, 0.0f ) )
                continue;
            for ( v2I = ( uhoVI + 1 ) % vCount; v2I != v1I; v2I = ( v2I + 1 ) % vCount ) {
                if ( tempArray[v2I].position == sf::Vector2f( 0.0f, 0.0f ) )
                    continue;
                else {
                    float vP = Utils::vectorProduct( tempArray[v1I].position - tempArray[uhoVI].position,
                            tempArray[v2I].position - tempArray[uhoVI].position );
                    if ( ( ( vP > 0 ) ? 1.0f : -1.0f ) == traversalDirection ) {
                        size_t k = 0;
                        for ( k = 0; k < tempArray.size(); k++ ) {
                            if ( tempArray[k].position == sf::Vector2f( 0.0f, 0.0f ) )
                                continue;
                            if ( k != v1I && k != uhoVI && k != v2I ) {
                                if ( Utils::inTriangle( tempArray[k].position, tempArray[v1I].position, tempArray[uhoVI].position,
                                        tempArray[v2I].position ) )
                                    break;
                            }
                        }
                        if ( k == tempArray.size() ) {
                            trianglesArray->append( tempArray[v1I] );
                            trianglesArray->append( tempArray[uhoVI] );
                            trianglesArray->append( tempArray[v2I] );
                            tempArray[uhoVI].position = sf::Vector2f( 0.0f, 0.0f );
                            realVCount--;
                            f = true;
                            break;
                        } else {
                            f = true;
                            v1I = uhoVI;
                            break;
                        }
                    } else {
                        v1I = uhoVI;
                        f = true;
                        break;
                    }
                    break;
                }
            }
            if ( f ) {
                f = false;
                break;
            }
        }
    }
    for ( size_t k = 0; k < tempArray.size(); k++ ) {
        if ( tempArray[k].position == sf::Vector2f( 0.0f, 0.0f ) )
            continue;
        trianglesArray->append( tempArray[k] );
    }
    goDef->drawables.push_back( trianglesArray );

    // b2BodyDef
    b2BodyDef bodyDef;
    bodyDef.position.Set( 0.0f, 0.0f );
    goDef->bodyDef = bodyDef;
}


ModelDef* ResourceManager::getModelDef( const string name ) {
    return _modelDefs[name];
}

float ResourceManager::getC() {
    return _c;
}

void ResourceManager::loadRect( rapidxml::xml_node<>* rectNode, sf::Vector2f borderSize, GameObjectDef* goDef ) {
    if ( strcmp( rectNode->first_attribute( "inkscape:label" )->value(), "rect" ) == 0 ) {
        float width = atof( rectNode->first_attribute( "width" )->value() );
        float height = atof( rectNode->first_attribute( "height" )->value() );
        float x = atof( rectNode->first_attribute( "x" )->value() );
        float y = atof( rectNode->first_attribute( "y" )->value() );
        b2PolygonShape* shape = new b2PolygonShape();
        shape->SetAsBox( width / 2 / _c, height / 2 / _c );
        goDef->fixture.shape = shape;
        sf::RectangleShape* rect = new sf::RectangleShape();
        rect->setOrigin( width / 2, height / 2 );
        rect->setPosition( sf::Vector2f( x, y ) + rect->getOrigin() );
        rect->setSize( sf::Vector2f( width, height ) );
        rect->setFillColor( sf::Color::Yellow );
        goDef->drawables.push_back( rect );
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        bodyDef.position.Set( x / _c + width / 2 / _c, ( borderSize.y - y - height / 2 ) / _c );
        goDef->bodyDef = bodyDef;
    }
    if ( strcmp( rectNode->first_attribute( "inkscape:label" )->value(), "circle" ) == 0 ) {
        float diameter = atof( rectNode->first_attribute( "width" )->value() );
        float x = atof( rectNode->first_attribute( "x" )->value() );
        float y = atof( rectNode->first_attribute( "y" )->value() );
        b2CircleShape* shape = new b2CircleShape();
        shape->m_radius = diameter / 2 / _c;
        goDef->fixture.shape = shape;
        sf::CircleShape* circle = new sf::CircleShape( diameter / 2, 30 );
        circle->setOrigin( diameter / 2, diameter / 2 );
        circle->setPosition( sf::Vector2f( x, y ) + circle->getOrigin() );
        circle->setFillColor( sf::Color::Green );
        goDef->drawables.push_back( circle );
        b2BodyDef bodyDef;
        bodyDef.type = b2_dynamicBody;
        bodyDef.position.Set( x / _c + diameter / 2 / _c, ( borderSize.y - y - diameter / 2 ) / _c );
        goDef->bodyDef = bodyDef;
    }
}

sf::Vector2f ResourceManager::getSVGSize( rapidxml::xml_node<>* firstNode ) {
    return sf::Vector2f( atoi( firstNode->first_attribute( "width" )->value() ), atoi( firstNode->first_attribute( "height" )->value() ) );
}

sf::Vector2f ResourceManager::getSVGPos( rapidxml::xml_node<>* firstNode ) {
    return sf::Vector2f( atof( firstNode->first_attribute( "x" )->value() ), atof( firstNode->first_attribute( "y" )->value() ) );
}

}
