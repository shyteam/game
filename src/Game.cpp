/*
 * Game.cpp
 *
 *  Created on: Oct 20, 2014
 *      Author: alexander
 */

#include "Game.h"

namespace gem {

Game::Game():
	_world( nullptr ),
	_window( nullptr ),
    _rm( nullptr ),
    _level( nullptr )
    {}

Game::~Game() {
	// TODO Auto-generated destructor stub
}



void Game::makeLevel( Level* level ) {
    _world = new World();
    _level = level;
	for ( auto i = level->groundDefs.begin(); i != level->groundDefs.end(); i++ ) {
		_world->createGameObject( *i );
	}
	for ( auto i = level->modelDefs.begin(); i != level->modelDefs.end(); i++ ) {
	     _world->createModel( *i );
	}
	_world->rm = _rm;
}

void Game::giveWindow( sf::RenderWindow* window ) {
	_window = window;
}

void Game::giveResourceManager( ResourceManager* rm ) {
    _rm = rm;
}

void Game::go() {
	while (_window->isOpen()) {
		_world->step();
		sf::Event event;
		while (_window->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				_window->close();
		}

		if ( sf::Keyboard::isKeyPressed( sf::Keyboard::Escape ) ) {
			_window->close();
		}
		sf::View v = _window->getView();
		v.setCenter( _world->getPlayer()->body->GetPosition().x*_rm->getC(), _level->size.y - _world->getPlayer()->body->GetPosition().y*_rm->getC() );
		_window->setView(v);
		_window->clear();
		for ( auto i = _world->_gameObjects.begin(); i != _world->_gameObjects.end(); i++ ) {
		    ( *i )->update( _rm->getC(), _level->size );
			_window->draw( *(*i) );
		}
		_window->display();
	}
}

} /* namespace gem */
