/*
 * Car.cpp
 *
 *  Created on: Nov 28, 2014
 *      Author: alexander
 */

#include <map>
#include <string>
#include "Car.h"

namespace gem {

Car::Car(): isPlayerInCar( false ) {}

Car::~Car() {}

void Car::act( GameObject* carrier ) {
	if ( isPlayerInCar ) {
		if (sf::Keyboard::isKeyPressed(Key["left"]) or
				sf::Keyboard::isKeyPressed(Key["left2"])) {
			carrier->body->ApplyTorque( carrier->params["torque"], true );
		}
		if (sf::Keyboard::isKeyPressed(Key["right"]) or
				sf::Keyboard::isKeyPressed(Key["right2"])) {
			carrier->body->ApplyTorque(-carrier->params["torque"], true );
		}
	    if ( sf::Keyboard::isKeyPressed( sf::Keyboard::F ) ) {
			//isPlayerInCar = false;
			//carrier->world->setPlayer( carrier->world->getModelById( "player", 0 )->gameObjects["body"] );
			/*ModelDef* playerDef = new ModelDef( *carrier->world->rm->getModelDef( "player" ) );
			playerDef->b2Pos = carrier->body->GetPosition() + b2Vec2( 0.0f, 1.0f );
			carrier->world->createModel( playerDef );*/
	    }
	} else {
		if ( sf::Keyboard::isKeyPressed( sf::Keyboard::F ) ) {
			//auto player = carrier->world->getPlayer();
			//if ( player != nullptr ) {
				isPlayerInCar = true;
				carrier->world->setPlayer( carrier );
				//delete player;
			//}
		}
	}
}

} /* namespace gem */
