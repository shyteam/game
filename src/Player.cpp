/*
 * Player.cpp
 *
 *  Created on: Oct 29, 2014
 *      Author: alexander
 */

#include "Player.h"

namespace gem {

Player::Player() {
    // TODO Auto-generated constructor stub
}

Player::~Player() {
    // TODO Auto-generated destructor stub
}

void Player::act( GameObject* carrier ) {
    if (sf::Keyboard::isKeyPressed(Key["up"]) or
            sf::Keyboard::isKeyPressed(Key["up2"])) {
        jump( carrier );
    }
    if (sf::Keyboard::isKeyPressed(Key["left"]) or
            sf::Keyboard::isKeyPressed(Key["left2"])) {
        moveLeft( carrier );
    }
    if (sf::Keyboard::isKeyPressed(Key["right"]) or
            sf::Keyboard::isKeyPressed(Key["right2"])) {
        moveRight( carrier );
    }
    if (sf::Keyboard::isKeyPressed(Key["down"]) or
            sf::Keyboard::isKeyPressed(Key["down2"])) {
        bend( carrier );
    }
}

void Player::jump( GameObject* carrier ) {
    carrier->body->ApplyForceToCenter(b2Vec2( 0.0f, 400.0f ), true);
}

void Player::moveLeft( GameObject* carrier ) {
    carrier->body->ApplyForceToCenter(b2Vec2( -200.0f, 0.0f ), true);
}

void Player::moveRight( GameObject* carrier ) {
    carrier->body->ApplyForceToCenter(b2Vec2( 200.0f, 0.0f ), true);
}

void Player::bend( GameObject* carrier ) {
    carrier->body->ApplyForceToCenter(b2Vec2( 0.0f, -200.0f ), true);
}

} /* namespace gem */
