/*
 * Wheel.cpp
 *
 *  Created on: Dec 7, 2014
 *      Author: alexander
 */

#include <Wheel.h>

namespace gem {

Wheel::Wheel() {
    // TODO Auto-generated constructor stub

}

Wheel::~Wheel() {
    // TODO Auto-generated destructor stub
}

void Wheel::act( GameObject* carrier ) {
    if ( ( ( Car* )carrier->model->gameObjects["body"]->soul )->isPlayerInCar ) {
        if (sf::Keyboard::isKeyPressed(Key["up"]) or
                sf::Keyboard::isKeyPressed(Key["up2"])) {
            carrier->body->ApplyTorque( carrier->params["torque"], true );
        }
        if (sf::Keyboard::isKeyPressed(Key["down"]) or
                sf::Keyboard::isKeyPressed(Key["down2"])) {
            carrier->body->ApplyTorque( -carrier->params["torque"], true );
        }
    }
}

} /* namespace gem */
