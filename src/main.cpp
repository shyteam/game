/*
 * main.cpp
 *
 *  Created on: Oct 20, 2014
 *      Author: alexander
 */


#include <SFML/Graphics.hpp>
//#include <SFGUI/SFGUI.hpp>
#include <Box2D/Box2D.h>
#include <iostream>
#include "Game.h"
#include "ResourceManager.h"

using namespace std;

int main()
{
    gem::ResourceManager rm;
    rm.loadPhysicMaterials( "/home/alexander/workspace/game/resources/PhysicMaterials.xml" );
    rm.loadModelDef(
            "/home/alexander/workspace/game/resources/ModelDefs/car.xml",
            "/home/alexander/workspace/game/resources/ModelDefs/car.svg" );

    rm.loadModelDef(
                "/home/alexander/workspace/game/resources/ModelDefs/car6.xml",
                "/home/alexander/workspace/game/resources/ModelDefs/car6.svg" );

    rm.loadModelDef(
                    "/home/alexander/workspace/game/resources/ModelDefs/quadrocar.xml",
                    "/home/alexander/workspace/game/resources/ModelDefs/quadrocar.svg" );

    rm.loadModelDef(
            "/home/alexander/workspace/game/resources/ModelDefs/player.xml",
            "/home/alexander/workspace/game/resources/ModelDefs/player.svg" );
    rm.loadModelDef(
                "/home/alexander/workspace/game/resources/ModelDefs/bycicle.xml",
                "/home/alexander/workspace/game/resources/ModelDefs/bycicle.svg" );

   // cout << rm.getModelDef( "car" )->gameObjectDefs["body"]->soulNames[0];
    rm.loadLevel( "/home/alexander/workspace/game/resources/Levels/lvl1.svg" );
    rm.loadLevel( "/home/alexander/workspace/game/resources/Levels/lvl2.svg" );
    rm.loadLevel( "/home/alexander/workspace/game/resources/Levels/lvl3.svg" );


	sf::ContextSettings settings;
	settings.antialiasingLevel = 16;
    std::vector<sf::VideoMode> modes = sf::VideoMode::getFullscreenModes();
    size_t sw = modes[0].width;
    size_t sh = modes[0].height;
    sf::View view( sf::FloatRect( 0, 0, sw, sh ) );
    sf::RenderWindow* window = new sf::RenderWindow( sf::VideoMode( sw, sh ), "BOMBA", sf::Style::Fullscreen, settings );
	window->setView(view);
	gem::Game game;
	game.giveWindow( window );
	game.giveResourceManager( &rm );
	game.makeLevel( rm.getLevel( "lvl1" ) );
	game.go();
	delete window;

	window = new sf::RenderWindow( sf::VideoMode( sw, sh ), "BOMBA", sf::Style::Fullscreen, settings );
	window->setView(view);
	game.giveWindow( window );
	game.makeLevel( rm.getLevel( "lvl2" ) );
	game.go();
	delete window;

    window = new sf::RenderWindow( sf::VideoMode( sw, sh ), "BOMBA", sf::Style::Fullscreen, settings );
    window->setView(view);
    game.giveWindow( window );
    game.makeLevel( rm.getLevel( "lvl3" ) );
    game.go();
    delete window;

    return 0;
}


