/*
 * GameObject.cpp
 *
 *  Created on: Oct 20, 2014
 *      Author: alexander
 */

#include "GameObject.h"

namespace gem {

GameObject::GameObject() :
        local_id ( "" ), model( nullptr ), soul( nullptr ), body( nullptr ), drawable( nullptr ), world( nullptr ) {
}

GameObject::~GameObject() {

}

void GameObject::draw( sf::RenderTarget& target, sf::RenderStates states ) const {
    target.draw( *drawable, states );
}

void gem::GameObject::update( float c, sf::Vector2f worldSize ) {
    if ( soul != 0 ) {
        soul->act( const_cast<GameObject*>( this ) );
    }
    if ( body->GetType() == b2_dynamicBody ) {
        ( ( sf::Shape* ) drawable )->setPosition( body->GetPosition().x * c, worldSize.y - body->GetPosition().y * c );
        ( ( sf::Shape* ) drawable )->setRotation( -body->GetAngle() * 180 / 3.14 );
    }
}

} /* namespace gem */
