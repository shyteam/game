/*
 * LevelLoader.cpp
 *
 *  Created on: Oct 20, 2014
 *      Author: alexander
 */

#include "Utils.h"

namespace gem {

gem::Utils::Line::Line():
	k( 0.0f ),
	b( 0.0f )
{}

gem::Utils::Line::Line(sf::Vector2f A, sf::Vector2f B):
	k( ( B.y - A.y ) / ( B.x - A.x ) ),
	b( ( B.x * A.y - A.x * B.y ) / ( B.x - A.x ) )
{}

POINT_POSITION gem::Utils::Line::pointIs(sf::Vector2f D) {
	if ( D.y < k * D.x + b )
		return POINT_POSITION::UNDER;
	if ( D.y == k * D.x + b )
		return POINT_POSITION::ON;
	return POINT_POSITION::ABOVE;
}

sf::Vector2f gem::Utils::Line::intersects( Line l ) {
	return sf::Vector2f( ( l.b - b ) / ( k -  l.k ), ( k * l.b - l.k * b ) / ( k - l.k ) );
}

bool Utils::Line::isPointInSection(sf::Vector2f X, sf::Vector2f A, sf::Vector2f B) {
	return ( std::max( A.x, B.x ) > X.x && std::min( A.x, B.x ) < X.x ) &&
			( std::max( A.y, B.y ) > X.y && std::min( A.y, B.y ) < X.y );
}


float gem::Utils::vectorProduct(sf::Vector2f A, sf::Vector2f B) {
	return A.x * B.y - A.y * B.x;
}

bool gem::Utils::inTriangle( sf::Vector2f V, sf::Vector2f A, sf::Vector2f B, sf::Vector2f C ) {
	Utils::Line lAB( A, B ), lBC( B, C ), lAC( A, C );
	if ( lAB.pointIs( V ) == ON || lBC.pointIs( V ) == ON || lAC.pointIs( V ) == ON )
		return true;
	bool res =
			( lAB.pointIs( C ) == lAB.pointIs( V ) ) &&
			( lBC.pointIs( A ) == lBC.pointIs( V ) ) &&
			( lAC.pointIs( B ) == lAC.pointIs( V ) );
	return res;
}

}/* namespace gem */

string gem::Utils::getFileName( const string filePath ) {
    return filePath.substr( filePath.find_last_of( "/" ) + 1, filePath.find_last_of( "." ) - filePath.find_last_of( "/" ) - 1 );
}
