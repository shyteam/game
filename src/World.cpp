/*
 * World.cpp
 *
 *  Created on: Oct 20, 2014
 *      Author: alexander
 */

#include "World.h"

namespace gem {

World::World():
	rm( nullptr ),
	_physicsWorld( b2Vec2( 0.0f, -10.0f ) ),
	_gameObjects(),
	_models(),
	_player( nullptr ),
    _souls(){
    _souls["player"] = new Player();
    _souls["car"] = new Car();
    _souls["wheel"] = new Wheel();
}

World::~World() {
	// TODO Auto-generated destructor stub
}

GameObject* World::createGameObject( GameObjectDef* gameObjectDef ) {
	b2Body* body = _physicsWorld.CreateBody( &gameObjectDef->bodyDef );
	body->CreateFixture( &gameObjectDef->fixture );
	GameObject* gameObject = new GameObject();
	gameObject->body = body;
	gameObject->drawable = gameObjectDef->drawables[0];
	gameObject->local_id = gameObjectDef->id;
	for ( auto i = gameObjectDef->soulNames.begin(); i != gameObjectDef->soulNames.end(); i++ ) {
	    gameObject->soul = _souls[*i];
	}
	gameObject->world = this;
	gameObject->params = gameObjectDef->params;
	gameObject->body->SetUserData( gameObject );
	_gameObjects.push_back( gameObject );
	return gameObject;
}

void World::step() {
	float32 timeStep = 1.0f / 60.0f;
	int32 velocityIterations = 6;
	int32 positionIterations = 2 ;
	_physicsWorld.Step( timeStep, velocityIterations, positionIterations );
}

Model* gem::World::createModel( ModelDef* mDef ) {
    string mFamily = mDef->family;
    size_t modelNumber = _models[mFamily].size();
	Model* model = new Model();
	model->family = mFamily;
	model->id = std::to_string( modelNumber );
	_models[mFamily][model->id] = model;
	for ( auto i = mDef->gameObjectDefs.begin(); i != mDef->gameObjectDefs.end(); i++ ) {
	    ( *i )->bodyDef.position += mDef->b2Pos;
	    GameObject* go = createGameObject( *i );
	    ( *i )->bodyDef.position -= mDef->b2Pos;
	    _player = go;
	    go->model = model;
	    model->gameObjects.insert( pair<const string, GameObject*>( go->local_id, go ) );
	}
	for ( auto i = mDef->jointDefs.begin(); i != mDef->jointDefs.end(); i++ ){
	    if ( ( *i )->type == e_revoluteJoint ) {
	        b2RevoluteJointDef* jD = new b2RevoluteJointDef( *( b2RevoluteJointDef* )( ( *i )->jointDef ) );
	        jD->Initialize( model->gameObjects[( *i )->bodyAid]->body, model->gameObjects[( *i )->bodyBid]->body,
	                ( ( b2RevoluteJointDef* )( *i )->jointDef )->localAnchorA + mDef->b2Pos );
	        jD->enableMotor = true;
	        jD->maxMotorTorque = 1.0f;
	        jD->motorSpeed = 0.0f;
	        _physicsWorld.CreateJoint( jD );
	    }
	}

	return model;
}

Model* gem::World::getModelById(const string family, size_t id ) {
	return _models[family][std::to_string( id )];
}

GameObject* gem::World::getPlayer() {
	return _player;
}

void gem::World::setPlayer(GameObject* newPlayer) {
	_player = newPlayer;
}

} /* namespace gem */

